import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class Tests {

    public char[][] map1 = makeMapExample1();
    public char[][] map2 = makeMapExample2();
    public char[][] map3 = makeMapExample3();
    public char[][] map4 = makeMapExample4();
    public char[][] map5 = makeMapExample5();
    public char[][] map6 = makeMapExample6();
    public char[][] map7 = makeMapExample7();

    public char[][] parseMap(String[] strings) {
        char [][] map = new char[strings.length][strings[0].length()];
        for (int i=0; i < strings.length; i++) {
            for (int j=0; j < strings[0].length(); j++)
                map[i][j] = strings[i].charAt(j);
        }
        return map;
    }

    public char[][] makeMapExample1() {
        String[] map = new String[5];
        map[0] = "...@.";
        map[1] = ".####";
        map[2] = ".....";
        map[3] = "####.";
        map[4] = ".X...";
        return parseMap(map);
    }

    public char[][] makeMapExample2() {
        String[] map = new String[5];
        map[0] = "..X..";
        map[1] = "#####";
        map[2] = ".....";
        map[3] = ".@...";
        map[4] = ".....";
        return parseMap(map);
    }

    public char[][] makeMapExample3() {
        String[] map = new String[5];
        map[0] = "....@";
        map[1] = "#.###";
        map[2] = ".....";
        map[3] = "....X";
        map[4] = ".....";
        return parseMap(map);
    }

    public char[][] makeMapExample4() {
        String[] map = new String[2];
        map[0] = "@";
        map[1] = "X";
        return parseMap(map);
    }

    public char[][] makeMapExample5() {
        String[] map = new String[1];
        map[0] = "/";
        return parseMap(map);
    }

    public char[][] makeMapExample6() {
        String[] map = new String[1];
        map[0] = "X";
        return parseMap(map);
    }

    public char[][] makeMapResult1() {
        String[] map = new String[5];
        map[0] = "+++@.";
        map[1] = "+####";
        map[2] = "+++++";
        map[3] = "####+";
        map[4] = ".X+++";
        return parseMap(map);
    }

    public char[][] makeMapResult3() {
        String[] map = new String[5];
        map[0] = ".+++@";
        map[1] = "#+###";
        map[2] = ".+...";
        map[3] = ".+++X";
        map[4] = ".....";
        return parseMap(map);
    }

    public char[][] makeMapExample7() {
        String[] map = new String[5];
        map[0] = ".....";
        map[1] = "X#.##";
        map[2] = ".#..@";
        map[3] = ".##.#";
        map[4] = "....#";
        return parseMap(map);
    }

    public char[][] makeMapResult7() {
        String[] map = new String[5];
        map[0] = "+++..";
        map[1] = "X#+##";
        map[2] = ".#++@";
        map[3] = ".##.#";
        map[4] = "....#";
        return parseMap(map);
    }

    @Test
    public void test1(){
        char[][] result1 = new BfsNavigator().searchRoute(map1);
        char[][] expectedResult1 = makeMapResult1();
        for (int i = 0; i < result1.length; i++)
            for (int j = 0; j < result1[0].length; j++)
                assertEquals(result1[i][j], expectedResult1[i][j]);
    }

    @Test
    public void test2(){
        char[][] result2 = new BfsNavigator().searchRoute(map2);
        assertNull(result2);
    }

    @Test
    public void test3(){
        char[][] result3 = new BfsNavigator().searchRoute(map3);
        char[][] expectedResult3 = makeMapResult3();
        for (int i = 0; i < result3.length; i++)
            for (int j = 0; j < result3[0].length; j++) {
                //System.out.print(result3[i][j] );
                //if (j == 4) System.out.println();
                assertEquals(result3[i][j], expectedResult3[i][j]);
            }
    }

    @Test
    public void test4(){
        char[][] result4 = new BfsNavigator().searchRoute(map4);
        for (int i = 0; i < result4.length; i++)
            for (int j = 0; j < result4[0].length; j++)
            assertEquals(result4[i][j], map4[i][j]);
    }

    @Test
    public void test5(){
        char[][] result5 = new BfsNavigator().searchRoute(map5);
        assertNull(result5);
    }

    @Test
    public void test6(){
        char[][] result6 = new BfsNavigator().searchRoute(map6);
        assertNull(result6);
    }

    @Test
    public void test7(){
        char[][] result7 = new BfsNavigator().searchRoute(map7);
        char[][] expectedResult7 = makeMapResult7();
        for (int i = 0; i < result7.length; i++)
            for (int j = 0; j < result7[0].length; j++){
                assertEquals(result7[i][j], expectedResult7[i][j]);
            }

    }
}
