public class BfsNavigator implements Navigator  {

    @Override
    public char[][] searchRoute(char[][] map) {

        return new NavigatorHelper(map).searchRoute();
    }
}