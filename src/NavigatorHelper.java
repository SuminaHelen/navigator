import javafx.util.Pair;

import java.util.ArrayDeque;
import java.util.Queue;

class NavigatorHelper {
    private char[][] map;
    private int rows;
    private int columns;
    private Point[][] track;
   // private Boolean[][] boolTrack;
    private Queue<Point> queue;


    public NavigatorHelper(char[][] map) {
        this.map = map;
        rows = map.length;
        columns = map[0].length;
        track = new Point[rows][columns];
        queue = new ArrayDeque<>(rows * columns);
    }

    public final static char entranceSymbol = '@';
    public final static char exitSymbol = 'X';
    public final static char wallSymbol = '#';
    public final static char pathSymbol = '+';

    private Pair<Boolean, Point> findTarget() {
        for(int i = 0; i < rows; i++)
            for(int j = 0; j < columns; j++) {
                char symbol = map[i][j];
                if (symbol == entranceSymbol)
                    return new Pair<>(true, new Point(i, j));
                if (symbol == exitSymbol)
                    return new Pair<>(false, new Point(i, j));
            }
        return null;
    }

    private boolean isInBounds(Point point) {
        return point.x >=0 && point.y >= 0 && point.x < rows && point.y < columns;
    }

    private void addPoint(Point old, int dx, int dy) {
        Point newPoint = new Point(dx + old.x, dy + old.y);
        if (!isInBounds(newPoint) || track[newPoint.x][newPoint.y] != null)
            return;
        queue.add(newPoint);
        track[newPoint.x][newPoint.y] = old;
    }

    private boolean tryDrawPath(Point start, boolean shouldSearchExit, char[][] map) {
        queue.add(start);
        track[start.x][start.y] = start;
        while (queue.size() > 0) {
            Point point = queue.poll();
            char pointChar = map[point.x][point.y];
            if (pointChar == wallSymbol)
                continue;

            if (((pointChar == exitSymbol) && shouldSearchExit) ||
                    ((pointChar == entranceSymbol) && !shouldSearchExit)) {
                Point exit = point;
                while (point != start) {
                    map[point.x][point.y] = pathSymbol;
                    point = track[point.x][point.y];
                }
                if (shouldSearchExit)
                    map[exit.x][exit.y] = exitSymbol;
                else
                    map[exit.x][exit.y] = entranceSymbol;
                return true;
            }
            addPoint(point, 0, -1);
            addPoint(point, 1, 0);
            addPoint(point, -1, 0);
            addPoint(point, 0, 1);

        }
        return false;
    }

    public char[][] searchRoute() {
        Pair<Boolean, Point> target = findTarget();
        if (target == null)
            return null;

        if(!tryDrawPath(target.getValue(), target.getKey(), map)) {
            return null;
        }

        return map;
    }
}
